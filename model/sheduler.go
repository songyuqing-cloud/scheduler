package models

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"time"
)

type Sheduler struct {
	ticker *time.Ticker
	ctx    context.Context
	cancel context.CancelFunc
}

func NewSheduler(interval time.Duration, ctx context.Context, cancel context.CancelFunc) *Sheduler {
	return &Sheduler{
		ticker: time.NewTicker(time.Second * interval),
		ctx:    ctx,
		cancel: cancel,
	}
}

func (s *Sheduler) Run() {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	go func() {
		for {
			select {
			case <-s.ticker.C:
				fmt.Println("Ticks Received..")
			case <-quit:
				fmt.Println("Sheduler shutting down")
				s.cancel()
			}
		}
	}()
}
