package main

import (
	"context"

	models "gitlab.com/artem-shestakov/scheduler/model"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	sheduler := models.NewSheduler(1, ctx, cancel)
	sheduler.Run()
	<-ctx.Done()
}
